package log4j;

import java.util.Random;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class TestLogGenerator {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		sleep(10000);
		PropertyConfigurator.configure(TestLogGenerator.class.getResource("log4j.properties"));
		Logger logger = Logger.getLogger("Logger1");
		Random rnd = new Random();
		while(true){
			int v = rnd.nextInt(100);
			String s = lorem(rnd.nextInt(20) + 1);
			if(v < 2){
				logger.fatal(s);
			}else if(v < 5){
				logger.error(s);
			}else if(v < 10){
				logger.warn(s);
			}else if(v < 80){
				logger.info(s);
			}else if(v < 90){
				logger.debug(s);
			}else{
				logger.trace(s);
			}
			sleep(rnd.nextInt(100));
		}
	}

	public static String lorem(int words) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < words; i++) {
            int n = random.nextInt(10);
            for (int j = 0; j < n; j++) {
                sb.append((char) (97 + random.nextInt(26)));
            }
            sb.append(' ');
        }
        return sb.toString();
    }

	public static void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
