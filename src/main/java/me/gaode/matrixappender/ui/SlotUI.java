package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.imagedata.CharacterBlock;
import me.gaode.matrixappender.imagedata.SlotData;
import me.gaode.matrixappender.imagedata.SlotDataHandler;


public class SlotUI implements SlotDataHandler {

    SlotData src;
    int leading;
    int charSize;
    int slotSize;
    private FadeEffect fade = new FadeEffect();
    private CopyChar copyChar = new CopyChar();

    public SlotUI(int slotSize) {
        this.slotSize = slotSize;
    }

    public void setSrc(SlotData src, int charSize, ColorScheme colorScheme) {
        this.src = src;
        this.leading = -1;
        this.copyChar.setCharSize(charSize);
        this.copyChar.setSrcSlot(src);
        this.fade.setColorScheme(colorScheme);
        this.charSize = Math.min(slotSize, charSize);
    }

    @Override
    public void processNextBlock(CharacterBlock block) {
        int index = block.getIndex();
        if (index == 0) {
            leading++;
        }
        if (leading < charSize) {
            if (index == leading) {
                block.process(copyChar);
            } else {
                block.process(fade);
            }
        }
    }

    public int getTime() {
        return leading - charSize;
    }

}
