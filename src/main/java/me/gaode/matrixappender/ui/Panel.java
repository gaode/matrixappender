package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.imagedata.ImageData;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public abstract class Panel {

    protected BufferedImage image;
    protected Point position;
    protected ImageData data;

    public Panel(int width, int height) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        byte[] b = ((DataBufferByte) (image.getRaster().getDataBuffer())).getData();
        data = new ImageData(b, width, height);
        this.position = new Point(0, 0);
    }

    public abstract void paint();

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setPosition(int x, int y) {
        this.position.setLocation(x, y);
    }

    public BufferedImage getImage() {
        return image;
    }

    public ImageData getData() {
        return data;
    }
}
