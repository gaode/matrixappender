package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.Configuration;

import java.awt.*;

public class ColorScheme {

    public static ColorScheme fatal;
    public static ColorScheme error;
    public static ColorScheme warn;
    public static ColorScheme info;
    public static ColorScheme debug;
    public static ColorScheme trace;

    static {
        reset();
    }

    public final Color start;
    public final Color step;
    public final Color residue;

    private ColorScheme(Color start, Color step, Color residue) {
        this.start = start;
        this.step = step;
        this.residue = residue;
    }

    public static void reset() {
        fatal = new ColorScheme(
                new Color(Configuration.colorFull, Configuration.colorFull, Configuration.colorFull),
                new Color(Configuration.colorStep, Configuration.colorStep, Configuration.colorStep),
                new Color(200, 200, 200));
        error = new ColorScheme(
                new Color(Configuration.colorFull, Configuration.colorFlash, Configuration.colorFlash),
                new Color(Configuration.colorStep, Configuration.colorFlashStep, Configuration.colorFlash),
                new Color(Configuration.colorResidue, 0, 0));
        warn = new ColorScheme(
                new Color(Configuration.colorFull, Configuration.colorFull, Configuration.colorFlash),
                new Color(Configuration.colorStep, Configuration.colorStep, Configuration.colorFlashStep),
                new Color(Configuration.colorResidue, Configuration.colorResidue, 0));
        info = new ColorScheme(
                new Color(Configuration.colorFlash, Configuration.colorFull, Configuration.colorFlash),
                new Color(Configuration.colorFlashStep, Configuration.colorStep, Configuration.colorFlashStep),
                new Color(0, Configuration.colorResidue, 0));
        debug = new ColorScheme(
                new Color(Configuration.colorFlash, Configuration.colorFlash, Configuration.colorFull),
                new Color(Configuration.colorFlashStep, Configuration.colorFlashStep, Configuration.colorStep),
                new Color(0, 0, Configuration.colorResidue));
        trace = new ColorScheme(
                new Color(Configuration.colorFlash, Configuration.colorFlash, Configuration.colorFlash),
                new Color(Configuration.colorStep, Configuration.colorStep, Configuration.colorStep),
                new Color(Configuration.colorResidue, Configuration.colorResidue, Configuration.colorResidue));
    }
}
