package me.gaode.matrixappender.ui.keylistener;

import java.awt.event.KeyEvent;

import static me.gaode.matrixappender.Configuration.refreshInterval;

public class RefreshSpeedListener extends SimpleKeyListener {

    public RefreshSpeedListener() {
        super(KeyEvent.VK_F6);
    }

    @Override
    public void up() {
        refreshInterval += 1;
    }

    @Override
    public void down() {
        refreshInterval -= 1;
        if (refreshInterval < 1) {
            refreshInterval = 1;
        }
    }

    @Override
    public void left() {
        refreshInterval = 20;
    }

    @Override
    public void right() {
    }

}
