package me.gaode.matrixappender.ui.keylistener;

import me.gaode.matrixappender.ui.ColorScheme;

import java.awt.event.KeyEvent;

import static me.gaode.matrixappender.Configuration.*;

public class ColorKeyListener extends SimpleKeyListener {

    public static final ColorKeyListener F1 = new ColorKeyListener(KeyEvent.VK_F1);
    public static final ColorKeyListener F2 = new ColorKeyListener(KeyEvent.VK_F2);
    public static final ColorKeyListener F3 = new ColorKeyListener(KeyEvent.VK_F3);
    public static final ColorKeyListener F4 = new ColorKeyListener(KeyEvent.VK_F4);
    public static final ColorKeyListener F5 = new ColorKeyListener(KeyEvent.VK_F5);
    protected ColorKeyListener(int keyCode) {
        super(keyCode);
    }

    @Override
    public void up() {
        switch (keyCode) {
            case KeyEvent.VK_F1:
                colorFull += 1;
                if (colorFull > 255) {
                    colorFull = 255;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F2:
                colorFlash += 1;
                if (colorFlash > 255) {
                    colorFlash = 255;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F3:
                colorStep += 1;
                if (colorStep > 255) {
                    colorStep = 255;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F4:
                colorFlashStep += 1;
                if (colorFlashStep > 255) {
                    colorFlashStep = 255;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F5:
                colorResidue += 10;
                if (colorResidue > 255) {
                    colorResidue = 255;
                }
                ColorScheme.reset();
                break;
        }
    }

    @Override
    public void down() {
        switch (keyCode) {
            case KeyEvent.VK_F1:
                colorFull -= 1;
                if (colorFull < 0) {
                    colorFull = 0;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F2:
                colorFlash -= 1;
                if (colorFlash < 0) {
                    colorFlash = 0;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F3:
                colorStep -= 1;
                if (colorStep < 0) {
                    colorStep = 0;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F4:
                colorFlashStep -= 1;
                if (colorFlashStep < 0) {
                    colorFlashStep = 0;
                }
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F5:
                colorResidue -= 10;
                if (colorResidue < 0) {
                    colorResidue = 0;
                }
                ColorScheme.reset();
                break;
        }
    }

    @Override
    public void left() {
        switch (keyCode) {
            case KeyEvent.VK_F1:
                colorFull = 250;
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F2:
                colorFlash = 200;
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F3:
                colorStep = 5;
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F4:
                colorFlashStep = 50;
                ColorScheme.reset();
                break;
            case KeyEvent.VK_F5:
                colorResidue = 0;
                ColorScheme.reset();
                break;
        }
    }

    @Override
    public void right() {
    }

}
