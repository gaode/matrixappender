package me.gaode.matrixappender.ui.keylistener;

import java.awt.event.KeyEvent;

public abstract class SimpleKeyListener implements PrimaryKeyListener {

    protected int keyCode;

    protected SimpleKeyListener(int keyCode) {
        this.keyCode = keyCode;
    }

    public int getPrimaryKey() {
        return keyCode;
    }


    public abstract void up();

    public abstract void down();

    public abstract void left();

    public abstract void right();

    public void onEvent(KeyEvent event) {
        switch (event.getKeyCode()) {
            case KeyEvent.VK_UP:
                up();
                break;
            case KeyEvent.VK_DOWN:
                down();
                break;
            case KeyEvent.VK_LEFT:
                left();
                break;
            case KeyEvent.VK_RIGHT:
                right();
                break;
        }
    }


}
