package me.gaode.matrixappender.ui.keylistener;

import me.gaode.matrixappender.Configuration;
import org.apache.log4j.Level;

import java.awt.event.KeyEvent;

public class LevelListener extends SimpleKeyListener {

    private static final int[] levels = new int[]{Level.TRACE_INT, Level.DEBUG_INT, Level.INFO_INT, Level.WARN_INT, Level.ERROR_INT, Level.FATAL_INT};

    public LevelListener() {
        super(KeyEvent.VK_F7);
    }

    @Override
    public void up() {
        Configuration.logLevel = nextLevel(Configuration.logLevel);
    }

    @Override
    public void down() {
        Configuration.logLevel = previousLevel(Configuration.logLevel);
    }

    @Override
    public void left() {
        Configuration.logLevel = Level.INFO_INT;
    }

    @Override
    public void right() {

    }

    private int nextLevel(int current) {
        for (int i = 0; i < levels.length; i++) {
            if (levels[i] > current) {
                return levels[i];
            }
        }
        return Level.FATAL_INT;
    }

    private int previousLevel(int current) {
        for (int i = levels.length - 1; i > -1; i--) {
            if (levels[i] < current) {
                return levels[i];
            }
        }
        return Level.TRACE_INT;
    }

}
