package me.gaode.matrixappender.ui.keylistener;

import java.util.Set;

public interface CombinationKeyListener {

    Set<Integer> getKeyCodes();

    void onEvent();
}
