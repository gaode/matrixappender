package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.imagedata.BlockRow;
import me.gaode.matrixappender.imagedata.CharacterBlockDataHandler;

public class FadeEffect implements CharacterBlockDataHandler {

    int rRes;
    int gRes;
    int bRes;
    int rStep;
    int gStep;
    int bStep;

    public void setColorScheme(ColorScheme colorScheme) {
        rRes = colorScheme.residue.getRed();
        gRes = colorScheme.residue.getGreen();
        bRes = colorScheme.residue.getBlue();
        rStep = colorScheme.step.getRed();
        gStep = colorScheme.step.getGreen();
        bStep = colorScheme.step.getBlue();
    }

    @Override
    public void processNextRow(BlockRow row) {
        byte[] data = row.data;
        for (int index = row.start; index < row.end; ) {
            int b = data[index];
            b &= 0xFF;
            if (b >= bRes) {
                b -= bStep;
                if (b < bRes) {
                    b = bRes;
                }
                data[index] = (byte) (b & 0xFF);
            }
            index++;
            int g = data[index];
            g &= 0xFF;
            if (g >= gRes) {
                g -= gStep;
                if (g < gRes) {
                    g = gRes;
                }
                data[index] = (byte) (g & 0xFF);
            }
            index++;
            int r = data[index];
            r &= 0xFF;
            if (r > rRes) {
                r -= rStep;
                if (r < rRes) {
                    r = rRes;
                }
                data[index] = (byte) (r & 0xFF);
            }
            index++;
        }
    }

}
