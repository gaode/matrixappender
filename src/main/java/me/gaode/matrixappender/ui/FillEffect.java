package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.imagedata.BlockRow;
import me.gaode.matrixappender.imagedata.CharacterBlockDataHandler;

public class FillEffect implements CharacterBlockDataHandler {


    @Override
    public void processNextRow(BlockRow row) {
        for (int i = row.start; i < row.end; i++) {
            row.data[i] = (byte) 100;
        }
    }

}
