package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.Configuration;

/**
 * Created by gaode on 08/11/2015.
 */
public class PaintLock {

    boolean isConnected = true;

    public void setConnected(boolean connected) {
        isConnected = connected;
        if (connected) {
            wakeUp();
        }
    }

    public synchronized void sleep() {
        try {
            int timeout = isConnected ? Configuration.refreshInterval : 0;
            wait(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void wakeUp() {
        notifyAll();
    }
}
