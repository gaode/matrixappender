package me.gaode.matrixappender.ui;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PanelStack {

    private List<Panel> panels = new ArrayList<Panel>();

    public void paint(Graphics g) {
        for (int i = 0; i < panels.size(); i++) {
            Panel panel = panels.get(i);
            g.drawImage(panel.getImage(), panel.getPosition().x, panel.getPosition().y, null);
        }
    }

    public void addPanel(Panel panel) {
        panels.add(panel);
    }

    public void removePanel(Panel panel) {
        panels.remove(panel);
    }
}
