package me.gaode.matrixappender.ui;

import me.gaode.matrixappender.imagedata.BlockRow;
import me.gaode.matrixappender.imagedata.CharacterBlockDataHandler;
import me.gaode.matrixappender.imagedata.SlotData;


public class CopyChar implements CharacterBlockDataHandler {

    SlotData srcSlot;
    int charSize;

    public void processNextRow(BlockRow row) {
        int charIndex = row.block.getIndex();
        if (charIndex < charSize) {
            BlockRow src = srcSlot.getBlock(charIndex).getRow(row.index);
            System.arraycopy(src.data, src.start, row.data, row.start, row.size);
        } else {
            for (int i = row.start; i < row.end; i++) {
                row.data[i] = 0;
            }
        }
    }

    public void setSrcSlot(SlotData srcSlot) {
        this.srcSlot = srcSlot;
    }

    public void setCharSize(int charSize) {
        this.charSize = charSize;
    }

}
