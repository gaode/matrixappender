package me.gaode.matrixappender;

import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

public class FifoQueue<T> {

    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private ReadLock readLock;
    private WriteLock writeLock;
    private LinkedList<T> objects;

    public FifoQueue() {
        readLock = lock.readLock();
        writeLock = lock.writeLock();
        objects = new LinkedList<T>();
    }

    public boolean put(T p) {
        writeLock.lock();
        try {
            objects.push(p);
            return objects.contains(p);
        } finally {
            writeLock.unlock();
        }
    }

    public boolean remove(T p) {
        writeLock.lock();
        try {
            return objects.remove(p);
        } finally {
            writeLock.unlock();
        }
    }

    public boolean contains(T p) {
        readLock.lock();
        try {
            return objects.contains(p);
        } finally {
            readLock.unlock();
        }
    }

    public T poll() {
        T o = null;
        writeLock.lock();
        try {
            o = objects.poll();
        } catch (NoSuchElementException nse) {
            // list is empty
        } finally {
            writeLock.unlock();
        }
        return o;
    }

    public int size() {
        readLock.lock();
        try {
            return objects.size();
        } finally {
            readLock.unlock();
        }
    }
}