package me.gaode.matrixappender;

import me.gaode.matrixappender.imagedata.ImageDataHandler;
import me.gaode.matrixappender.imagedata.SlotData;
import me.gaode.matrixappender.ui.Panel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MainPanel extends Panel implements ImageDataHandler {

    List<Slot> slots = new ArrayList<Slot>();
    Slot[] slota;

    public MainPanel() {
        super(ScreenHelper.getDisplayMode().getWidth(), ScreenHelper.getDisplayMode().getHeight());
        int slotSize = data.getSlotData().get(0).getBlocks().size();
        for (int i = 0; i < data.getSlotData().size(); i++) {
            slots.add(new Slot(slotSize));
        }
        slota = slots.toArray(new Slot[]{});
    }

    public void paint() {
        data.process(this);
    }

    public Slot getAvailableSlot(String name) {
        Arrays.sort(slota);
        Slot slot = null;
        for (int i = slota.length - 1; i > -1; i--) {
            if (slota[i].getUI().getTime() > 0
                    && (slota[i].getName() == null
                    || slota[i].getName().equals(name))) {
                slot = slota[i];
                break;
            }
        }
        if (slot == null) {
            for (int i = slota.length - 1; i > -1; i--) {
                if (slota[i].getUI().getTime() > 0) {
                    slot = slota[i];
                    break;
                }
            }
        }
        return slot;
    }

    @Override
    public void processNextSlot(SlotData slotData) {
        int index = slotData.getIndex();
        Slot slot = slots.get(index);
        slotData.process(slot.getUI());
    }

    public Slot getOldestSlot() {
        Arrays.sort(slota);
        return slota[slota.length - 1];
    }

    public Slot[] getSortedSlots() {
        Arrays.sort(slota);
        return slota;
    }
}
