package me.gaode.matrixappender;

import org.apache.log4j.Level;

public class Configuration {

    public static final String _FILENAME = "config.properties";
    public static int logServerPort = 8887;

    public static int colorStep = 5;
    public static int colorResidue = 0;
    public static int colorFlash = 200;
    public static int colorFlashStep = 50;
    public static int colorFull = 250;

    public static int refreshInterval = 20;
    public static int logLevel = Level.INFO.toInt();

}
