package me.gaode.matrixappender;

import me.gaode.matrixappender.ui.Panel;

import java.awt.*;

public class SystemFailurePanel extends Panel {

    private static final int H = 100;
    private static final int W = 460;

    public SystemFailurePanel() {
        super(W, H);
        DisplayMode mode = ScreenHelper.getDisplayMode();
        mode.getWidth();
        this.setPosition((mode.getWidth() - W) / 2, (mode.getHeight() - H) / 2);
        Graphics g = super.image.getGraphics();
        Graphics2D g2 = (Graphics2D) g;
//		g2.setColor(Color.white);
//		g2.fillRect(0, 0, W, H);
        g2.setColor(new Color(0, 150, 0));
        g2.setStroke(new BasicStroke(5.0f));
        g2.drawRect(10, 10, W - 20, H - 20);
        g2.setFont(new Font("Courier New", Font.BOLD, 48));
        g2.drawString("SYSTEM FAILURE", 25, 64);
    }

    @Override
    public void paint() {
    }

}
