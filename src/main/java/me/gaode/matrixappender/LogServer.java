package me.gaode.matrixappender;

import org.apache.log4j.spi.LoggingEvent;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class LogServer {

    Thread t;
    private boolean isStarted = false;
    private LogListener listener;

    public synchronized void start() {
        if (isStarted) {
            return;
        }
        isStarted = true;
        t = new Thread() {
            public void run() {
                ServerSocket ss = null;
                try {
                    ss = new ServerSocket(Configuration.logServerPort);
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    System.exit(1);
                }
                while (true) {
                    try (Socket socket = ss.accept()){
                        listener.connected();
                        ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                        while (true) {
                            LoggingEvent event = (LoggingEvent) ois.readObject();
                            listener.onEvent(event);
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        listener.disconnected();
                    }
                }
            }
        };
        t.start();
    }

    @SuppressWarnings("deprecation")
    public void stop() {
        if (isStarted) {
            t.stop();
        }
    }

    public void setListener(LogListener listener) {
        this.listener = listener;
    }
}
