package me.gaode.matrixappender;

import org.apache.log4j.spi.LoggingEvent;

public interface LogListener {

    void connected();

    void onEvent(LoggingEvent event);

    void disconnected();
}
