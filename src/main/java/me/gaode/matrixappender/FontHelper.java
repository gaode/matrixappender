package me.gaode.matrixappender;

import java.awt.*;
import java.awt.image.BufferedImage;


public class FontHelper {

    public static int FONT_WIDTH;
    public static int FONT_HEIGHT;

    static String FONT_NAME = "Courier New";
    static int FONT_SIZE = 14;
    static Graphics g;
    static FontMetrics fm;
    static Font font;

    static {
        BufferedImage bi = new BufferedImage(1, 1, BufferedImage.TYPE_3BYTE_BGR);
        g = bi.getGraphics();
        g.setFont(new Font("§", Font.PLAIN, FONT_SIZE));
        fm = g.getFontMetrics();
    }

    public static BufferedImage drawString(String s, Color color) {
        int[] d = getDimension(s);
        BufferedImage bi = new BufferedImage(d[0], d[1], BufferedImage.TYPE_3BYTE_BGR);
        Graphics graphics = bi.getGraphics();
        graphics.setColor(color);
        graphics.setFont(font);
        graphics.drawString(s, 0, d[1] - fm.getDescent());
        return bi;
    }

    public static void setFontSize(int fontSize) {
        FONT_SIZE = fontSize;
        font = new Font(FONT_NAME, Font.PLAIN, FONT_SIZE);
        g.setFont(font);
        fm = g.getFontMetrics();
        FONT_WIDTH = fm.stringWidth("A");
        FONT_HEIGHT = fm.getHeight();
    }

    private static int[] getDimension(String str) {
        int[] c = new int[2];
        c[0] = fm.stringWidth(str);
        c[1] = fm.getHeight();
        return c;
    }
}
