package me.gaode.matrixappender;

import me.gaode.matrixappender.imagedata.ImageData;
import me.gaode.matrixappender.imagedata.SlotData;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

public class ScreenHelper {

    private static GraphicsDevice screenDevice;
    private static DisplayMode displayMode;

    public static void createFullScreenWindow(Window window) {
        GraphicsDevice gd = getScreenDevice();
        Rectangle bounds = gd.getDefaultConfiguration().getBounds();
        window.setBounds(bounds);
        window.setVisible(true);
    }

    public static DisplayMode getDisplayMode() {
        if (displayMode == null) {
            displayMode = getScreenDevice().getDisplayMode();
        }
        return displayMode;
    }

    private static synchronized GraphicsDevice getScreenDevice() {
        if (screenDevice == null) {
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] screenDevices = ge.getScreenDevices();
            screenDevice = ge.getDefaultScreenDevice();
            Rectangle defaultBounds = screenDevice.getDefaultConfiguration().getBounds();
            if (screenDevices.length > 1) {
                for (GraphicsDevice device : screenDevices) {
                    Rectangle bounds = device.getDefaultConfiguration().getBounds();
                    if (bounds.x > defaultBounds.x) {
                        screenDevice = device;
                        defaultBounds = bounds;
                    }
                }
            }
        }
        return screenDevice;
    }

}
