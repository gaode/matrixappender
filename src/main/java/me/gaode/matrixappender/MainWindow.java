package me.gaode.matrixappender;

import me.gaode.matrixappender.ui.Panel;
import me.gaode.matrixappender.ui.PanelStack;
import me.gaode.matrixappender.ui.keylistener.CombinationKeyListener;
import me.gaode.matrixappender.ui.keylistener.PrimaryKeyListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MainWindow extends JFrame implements KeyListener {

    private static final long serialVersionUID = -6001655763295857307L;

    PanelStack panels = new PanelStack();
    Map<Integer, PrimaryKeyListener> primaryKeyListeners = new HashMap<Integer, PrimaryKeyListener>();
    Map<Set<Integer>, CombinationKeyListener> combinationKeyListeners = new HashMap<Set<Integer>, CombinationKeyListener>();
    Set<Integer> keys = new HashSet<Integer>();
    boolean suppressKeyRelease = false;

    public MainWindow() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setFocusable(true);
        this.setFocusableWindowState(true);
        this.setFocusTraversalKeysEnabled(false);
        this.setUndecorated(true);
//		this.setAlwaysOnTop(true);
        this.addKeyListener(this);
    }

    public void paint(Graphics g) {
        panels.paint(g);
    }

    public void addPanel(Panel panel) {
        panels.addPanel(panel);
    }

    public void removePanel(Panel panel) {
        panels.removePanel(panel);
    }

    public void addPrimaryKeyListener(PrimaryKeyListener listener) {
        primaryKeyListeners.put(listener.getPrimaryKey(), listener);
    }

    public void removePrimaryKeyListener(PrimaryKeyListener listener) {
        primaryKeyListeners.remove(listener.getPrimaryKey());
    }

    public void addCombinationKeyListener(CombinationKeyListener listener) {
        combinationKeyListeners.put(listener.getKeyCodes(), listener);
    }

    public void removeCombinationKeyListener(CombinationKeyListener listener) {
        combinationKeyListeners.remove(listener.getKeyCodes());
    }

    public void keyTyped(KeyEvent e) {
    }

    public synchronized void keyPressed(KeyEvent e) {
        // System.out.println(e.getKeyCode());
        keys.add(e.getKeyCode());
        if (suppressKeyRelease) {
            suppressKeyRelease = false;
        }
        if (keys.size() > 1) {
            CombinationKeyListener listener = combinationKeyListeners.get(keys);
            if (listener != null) {
                suppressKeyRelease = true;
                listener.onEvent();
            }
        }
    }

    public synchronized void keyReleased(KeyEvent e) {
        int keyCode = e.getKeyCode();
        keys.remove(keyCode);
        if (suppressKeyRelease) {
            return;
        }
        if (keys.size() == 1) {
            int key = keys.iterator().next();
            PrimaryKeyListener listener = primaryKeyListeners.get(key);
            if (listener != null) {
                listener.onEvent(e);
            }
        }
    }
}
