package me.gaode.matrixappender;

import me.gaode.matrixappender.imagedata.SlotData;
import me.gaode.matrixappender.ui.ColorScheme;
import me.gaode.matrixappender.ui.SlotUI;
import org.apache.log4j.Level;
import org.apache.log4j.spi.LoggingEvent;

public class Slot implements Comparable<Slot> {

    SlotUI slotUI;

    String name;

    public Slot(int slotSize) {
        this.slotUI = new SlotUI(slotSize);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SlotUI getUI() {
        return slotUI;
    }

    public void showEvent(LoggingEvent event) {
        ColorScheme colorScheme = ColorScheme.info;
        if (event.getLevel().equals(Level.TRACE)) {
            colorScheme = ColorScheme.trace;
        } else if (event.getLevel().equals(Level.DEBUG)) {
            colorScheme = ColorScheme.debug;
        } else if (event.getLevel().equals(Level.INFO)) {
            colorScheme = ColorScheme.info;
        } else if (event.getLevel().equals(Level.WARN)) {
            colorScheme = ColorScheme.warn;
        } else if (event.getLevel().equals(Level.ERROR)) {
            colorScheme = ColorScheme.error;
        } else if (event.getLevel().equals(Level.FATAL)) {
            colorScheme = ColorScheme.fatal;
        }
        this.name = event.getLoggerName();
        String message = event.getMessage().toString();
        SlotData slotData = SlotData.toSlotData(message, colorScheme.start);
        slotUI.setSrc(slotData, message.length(), colorScheme);
    }

    @Override
    public int compareTo(Slot o) {
        return slotUI.getTime() - o.getUI().getTime();
    }

}
