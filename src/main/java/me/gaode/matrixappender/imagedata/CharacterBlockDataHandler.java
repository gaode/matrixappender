package me.gaode.matrixappender.imagedata;

public interface CharacterBlockDataHandler {

    void processNextRow(BlockRow row);
}
