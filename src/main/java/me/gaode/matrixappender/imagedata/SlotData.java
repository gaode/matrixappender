package me.gaode.matrixappender.imagedata;

import me.gaode.matrixappender.FontHelper;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.List;

public class SlotData {

    public int height;
    ImageData imageData;
    int slotIndex;
    int blockCount;
    int width;
    private List<CharacterBlock> blocks = new ArrayList<CharacterBlock>();

    public SlotData(ImageData imageData, int slotIndex, int width, int height) {
        this.imageData = imageData;
        this.slotIndex = slotIndex;
        this.height = height;
        this.width = width;
        this.blockCount = width / FontHelper.FONT_WIDTH;
        for (int i = 0; i < blockCount; i++) {
            blocks.add(new CharacterBlock(this, i));
        }
    }

    public static SlotData toSlotData(String message, Color color) {
        BufferedImage img = FontHelper.drawString(message, color);
        ImageData imgData = new ImageData(((DataBufferByte) img.getRaster().getDataBuffer()).getData(), img.getWidth(), img.getHeight());
        SlotData slotData = imgData.getSlotData().get(0);
        return slotData;
    }

    public int getStart() {
        return slotIndex * width * 3 * height;
    }

    public void process(SlotDataHandler handler) {
        for (CharacterBlock block : blocks) {
            handler.processNextBlock(block);
        }
    }

    public int size() {
        return width * height;
    }

    public CharacterBlock getBlock(int index) {
        return blocks.get(index);
    }

    public List<CharacterBlock> getBlocks() {
        return blocks;
    }

    public int getIndex() {
        return slotIndex;
    }
}
