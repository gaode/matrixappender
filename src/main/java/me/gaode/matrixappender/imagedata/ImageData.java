package me.gaode.matrixappender.imagedata;

import me.gaode.matrixappender.FontHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * 3-bytes BGR image data;
 * TODO: show 'SYSTEM FAILURE' on IOException in socket
 * TODO: fly speed is controlled by the waiting list size: mapped by 指数映射
 * TODO: min value left for display
 *
 * @author gaode
 */
public class ImageData {

    public byte[] data;
    int width;
    int height;
    int slotHeight;
    int slotCount;

    private List<SlotData> slots = new ArrayList<SlotData>();

    public ImageData(byte[] data, int width, int height, int fontWidth, int fontHeight) {
        super();
        this.data = data;
        this.width = width;
        this.height = height;
        this.slotHeight = fontHeight;
        this.slotCount = height / fontHeight;
        for (int i = 0; i < slotCount; i++) {
            slots.add(new SlotData(this, i, width, slotHeight));
        }
    }

    public ImageData(byte[] data, int width, int height) {
        this(data, width, height, FontHelper.FONT_WIDTH, FontHelper.FONT_HEIGHT);
    }

    public List<SlotData> getSlotData() {
        return slots;
    }

    public void process(ImageDataHandler handler) {
        for (SlotData slot : slots) {
            handler.processNextSlot(slot);
        }
    }
}
