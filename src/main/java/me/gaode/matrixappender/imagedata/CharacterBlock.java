package me.gaode.matrixappender.imagedata;

import me.gaode.matrixappender.FontHelper;

import java.util.ArrayList;
import java.util.List;

public class CharacterBlock {

    SlotData slotData;
    int width;
    int height;
    int index;
    int size;
    int start;
    List<BlockRow> rows = new ArrayList<BlockRow>();

    public CharacterBlock(SlotData slotData, int index) {
        this.slotData = slotData;
        this.index = index;
        this.width = FontHelper.FONT_WIDTH;
        this.height = slotData.height;
        this.size = this.width * this.height;
        this.start = slotData.getStart() + index * width * 3;
        int rowStart = start;
        int rowSize = width * 3;
        for (int i = 0; i < height; i++) {
            this.rows.add(new BlockRow(this, rowStart, rowSize, i));
            rowStart += slotData.width * 3;
        }
    }

    public void process(CharacterBlockDataHandler handler) {
        for (BlockRow row : rows) {
            handler.processNextRow(row);
        }
    }

    public int getIndex() {
        return index;
    }

    public int size() {
        return size;
    }

    public int getStart() {
        return start;
    }

    public BlockRow getRow(int index) {
        return rows.get(index);
    }
}
