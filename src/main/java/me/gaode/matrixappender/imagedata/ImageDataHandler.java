package me.gaode.matrixappender.imagedata;

public interface ImageDataHandler {

    void processNextSlot(SlotData slot);
}
