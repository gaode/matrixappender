package me.gaode.matrixappender.imagedata;

public class BlockRow {

    public final byte[] data;
    public final int start;
    public final int end;
    public final int size;
    public final int index;
    public final CharacterBlock block;

    public BlockRow(CharacterBlock block, int start, int size, int index) {
        super();
        this.block = block;
        this.data = block.slotData.imageData.data;
        this.start = start;
        this.end = start + size;
        this.size = size;
        this.index = index;
    }

}
