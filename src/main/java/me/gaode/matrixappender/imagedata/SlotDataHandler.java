package me.gaode.matrixappender.imagedata;

public interface SlotDataHandler {

    void processNextBlock(CharacterBlock block);
}
