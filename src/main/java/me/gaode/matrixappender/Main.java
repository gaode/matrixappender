package me.gaode.matrixappender;


import me.gaode.matrixappender.ui.PaintLock;
import me.gaode.matrixappender.ui.keylistener.ColorKeyListener;
import me.gaode.matrixappender.ui.keylistener.ExitListener;
import me.gaode.matrixappender.ui.keylistener.LevelListener;
import me.gaode.matrixappender.ui.keylistener.RefreshSpeedListener;
import org.apache.log4j.spi.LoggingEvent;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IllegalAccessException, IOException, InstantiationException {
        ConfigurationLoader.load(Configuration.class);
        FontHelper.setFontSize(24);
        final MainWindow window = new MainWindow();
        final MainPanel mainPanel = new MainPanel();
        final SystemFailurePanel systemFailurePanel = new SystemFailurePanel();
        final PaintLock paintLock = new PaintLock();
        window.addPanel(mainPanel);
        window.addPrimaryKeyListener(ColorKeyListener.F1);
        window.addPrimaryKeyListener(ColorKeyListener.F2);
        window.addPrimaryKeyListener(ColorKeyListener.F3);
        window.addPrimaryKeyListener(ColorKeyListener.F4);
        window.addPrimaryKeyListener(ColorKeyListener.F5);
        window.addPrimaryKeyListener(new RefreshSpeedListener());
        window.addCombinationKeyListener(new ExitListener());
        window.addPrimaryKeyListener(new LevelListener());
        ScreenHelper.createFullScreenWindow(window);

        final FifoQueue<LoggingEvent> queue = new FifoQueue<LoggingEvent>();
        LogServer server = new LogServer();
        server.setListener(new LogListener() {
            public void connected() {
                window.removePanel(systemFailurePanel);
                paintLock.setConnected(true);
            }

            public void onEvent(LoggingEvent event) {
                queue.put(event);
            }

            public void disconnected() {
                window.addPanel(systemFailurePanel);
                paintLock.setConnected(false);
            }
        });
        server.start();
        LoggingEvent event = null;
        while (true) {
            while (true) {
                if (queue.size() > 0) {
                    event = queue.poll();
                } else {
                    break;
                }
                Slot slot = mainPanel.getAvailableSlot(event.getLoggerName());
                if (slot == null) {
                    break;
                }
                if (event.getLevel().toInt() >= Configuration.logLevel) {
                    slot.showEvent(event);
                }
            }
            mainPanel.paint();
            window.repaint();
            paintLock.sleep();
        }
    }

}
