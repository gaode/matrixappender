package me.gaode.matrixappender;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map.Entry;
import java.util.Properties;

class ConfigurationLoader {

    private InputStream inputStream;
    private final Class clazz;
    private Object configObj;

    private ConfigurationLoader(Class clazz) {
        this.clazz = clazz;
        try {
            Field field = clazz.getDeclaredField("_FILENAME");
            field.setAccessible(true);
            Object fileName = field.get(null);
            if (fileName != null) {
                this.inputStream = clazz.getResourceAsStream("/" + fileName.toString());
            }
        } catch (SecurityException | IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(
                    "Configuration class must have a _FILENAME filed defined.");
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("The _FILENAME field defined in configuration class must be static and has a string value");
        }
    }

    private String set(String key, String value) {
        String name = getFieldName(key);
        try {
            Field field;
            try {
                field = clazz.getDeclaredField(name);
            } catch (NoSuchFieldException e) {
                return "No such key" + key;
            }
            if (!isValid(field)) {
                return "No such key" + key;
            }
            boolean canAccess = field.isAccessible();
            if (!canAccess) {
                field.setAccessible(true);
            }
            Class<?> type = field.getType();
            if (type == int.class || type == Integer.class) {
                field.setInt(configObj, Integer.parseInt(value));
            } else if (type == double.class || type == Double.class) {
                field.setDouble(configObj, Double.parseDouble(value));
            } else if (type == float.class || type == Float.class) {
                field.setFloat(configObj, Float.parseFloat(value));
            } else if (type == long.class || type == Long.class) {
                field.setLong(configObj, Long.parseLong(value));
            } else if (type == short.class || type == Short.class) {
                field.setShort(configObj, Short.parseShort(value));
            } else if (type == boolean.class || type == Boolean.class) {
                field.setBoolean(configObj, Boolean.parseBoolean(value));
            } else if (type == char.class || type == Character.class) {
                field.setChar(configObj, value.charAt(0));
            } else if (type == byte.class || type == Byte.class) {
                field.setByte(configObj, value.getBytes()[0]);
            } else if (type == String.class) {
                field.set(configObj, value);
            }
            if (!canAccess) {
                field.setAccessible(false);
            }
            return null;
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public static void load(Class<?> clazz) {
        ConfigurationLoader loader = new ConfigurationLoader(clazz);
        loader.loadConfig();
    }

    private void loadConfig() {
        if (this.inputStream == null) {
            return;
        }
        try {
            configObj = clazz.newInstance();
            Properties prop = new Properties();
            prop.load(this.inputStream);
            for (Entry<Object, Object> entry : prop.entrySet()) {
                String key = entry.getKey().toString().trim();
                String value = entry.getValue().toString().trim();
                if (value.length() == 0)
                    continue;
                set(key, value);
            }
            this.inputStream.close();
        } catch (InstantiationException | IllegalAccessException | IOException e) {
            e.printStackTrace();
        }
    }

    private boolean isValid(Field field) {
        Class<?> type = field.getType();
        return (type == int.class || type == Integer.class || type == String.class || type == double.class || type == Double.class || type == float.class || type == Float.class || type == long.class || type == Long.class || type == short.class || type == Short.class || type == boolean.class || type == Boolean.class || type == char.class || type == Character.class || type == byte.class || type == Byte.class) && !field.getName().contains("_") && !Modifier.isFinal(field.getModifiers());
    }

    private String getFieldName(String key) {
        StringBuilder sb = new StringBuilder();
        char[] ca = key.toCharArray();
        boolean up = false;
        for (char ch : ca) {
            if (ch == '.') {
                up = true;
                continue;
            }
            if (up) {
                sb.append(Character.toUpperCase(ch));
                up = false;
            } else {
                sb.append(ch);
            }
        }
        return sb.toString();
    }
}
