Matrix Appender
===============

Introduction
------------
Matrix Appender is a log4j socket appender that output the log message in a full screen window (on the
far right screen if the host has two screens). The message has the falling character effect as in the 
movie 'The Matrix'.  
This is not a screen saver. It is a working log4j socket appender.

Quick Start
-----------
Run class me.gaode.matrixappender.Main. It will create a log4j socket appender and listen on port 8887
on current host.

To configure the log4j socket appender, you can add a new appender in your log4j configuration file like
below:

log4j.appender.MATRIX=org.apache.log4j.net.SocketAppender  
log4j.appender.MATRIX.RemoteHost= *the host that you start the matrix appender*  
log4j.appender.MATRIX.Port=8887  
log4j.appender.MATRIX.ReconnectionDelay=3000

Then start your app and watch the show.

Command Keys
------------
There are some predefined keys for the appender:

1. F1~F5 + UP/DOWN: change color scheme
2. F1~F5 + LEFT: reset color scheme to default
3. F6 + UP/DOWN: change refresh rate
4. F6 + LEFT: reset refresh rate
5. F7 + UP/DOWN: change log level
6. F7 + LEFT: reset log level to INFO
7. QUIT: terminate appender